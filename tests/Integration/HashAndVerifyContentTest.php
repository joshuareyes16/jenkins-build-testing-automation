<?php

use LordDashMe\Hasher\Hasher;
use PHPUnit\Framework\TestCase;

class HashAndVerifyContentTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_hash_and_verify_if_match_the_given_content()
    {
        $constructorParameter = [
            PASSWORD_BCRYPT,
            []
        ];

        $hashed = ($this->concreteClass($constructorParameter))
            ->hash($this->dummyContent())
            ->get();

        $matched = Hasher::verify($this->dummyContent(), $hashed);

        $this->assertTrue($matched);     
    }

    protected function dummyContent()
    {
        return 'Integration testing for hasher.';  
    }

    protected function concreteClass($args = null)
    {
        if (is_array($args)) {
            return new Hasher(...$args);
        }

        return new Hasher();
    }  
}
