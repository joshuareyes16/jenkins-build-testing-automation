<?php

use LordDashMe\Hasher\Hasher;
use PHPUnit\Framework\TestCase;

class HasherTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_load_concrete_class()
    {
        $this->assertInstanceOf(LordDashMe\Hasher\Hasher::class, $this->concreteClass());
    }

    /**
     * @test
     */
    public function it_can_get_info_of_the_given_hashed_content()
    {
        $hashedInfo = Hasher::getInfo($this->dummyHashedContent());

        $this->assertTrue(is_array($hashedInfo));
    }

    /**
     * @test
     */
    public function it_can_verify_the_given_content_match_the_hashed_content()
    {
        $true = Hasher::verify($this->dummyContent(), $this->dummyHashedContent());

        $this->assertTrue($true); 
    }

    /** 
     * @test
     */
    public function it_can_hash_give_content()
    {
        $constructorParameter = [
            PASSWORD_BCRYPT,
            []
        ];

        $hashed = ($this->concreteClass($constructorParameter))
            ->hash($this->dummyContent())
            ->get();

        $this->assertTrue(is_string($hashed));
    }

    /**
     * @test
     */
    public function it_can_rehash_old_hashed_content()
    {
        $constructorParameter = [
            PASSWORD_BCRYPT,
            ['cost' => 6]
        ];

        $newHashed = ($this->concreteClass($constructorParameter))
            ->rehash($this->dummyContent(), $this->dummyHashedContent())
            ->get();

        $this->assertNotEquals($this->dummyHashedContent(), $newHashed);    
    }

    protected function dummyContent()
    {
        return 'Need to be hash';  
    }

    protected function dummyHashedContent()
    {
        return '$2y$10$cwzwDA.wXJitJMPQt9ogDe5rf46dASXh8r5DPIyH1Up3HhhROcFti';
    }

    protected function concreteClass($args = null)
    {
        if (is_array($args)) {
            return new Hasher(...$args);
        }

        return new Hasher();
    }  
}
