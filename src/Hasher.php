<?php

namespace LordDashMe\Hasher;

class Hasher
{
    protected $algo;
    protected $options;

    protected $hashedData;

    public function __construct($algo = PASSWORD_DEFAULT, $options = [])
    {
        $this->algo = $algo;
        $this->options = $options;
    }

    public static function getInfo($hashedContent)
    {
        return password_get_info($hashedContent);
    }

    public static function verify($content, $hashedContent)
    {
        return password_verify($content, $hashedContent);   
    }

    public function hash($content)
    {
        $this->hashedData = password_hash($content, $this->algo, $this->options);

        return $this;
    }

    public function rehash($content, $hash)
    {
        if (password_needs_rehash($hash, $this->algo, $this->options)) {
            $this->hash($content);
        }

        return $this;
    }

    public function trashMethod()
    {   
        if (true) {
            return true;
        }

        return false;
    }

    public function anotherTrashMethod()
    {
        return "nothing else";
    }

    public function get()
    {
        return $this->hashedData;
    }
}